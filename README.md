[![Hits](https://hits.seeyoufarm.com/api/count/incr/badge.svg?url=https%3A%2F%2Fgitlab.com%2Fahnshy&count_bg=%23FF4000&title_bg=%23000000&icon=&icon_color=%23E7E7E7&title=Today&edge_flat=false)](https://hits.seeyoufarm.com) <br/> 
[![dev_ahnshy_intro](https://github.com/user-attachments/assets/1f900066-1692-4797-bfb0-61b447439b46)](https://www.youtube.com/watch?v=D07QdP161BQ?t=0s)

<!-- About Link -->
## Link
<a href="https://ahnshy.github.io/" target="_blank"><img src="https://img.shields.io/badge/github.io-gray?logo=github" /></a>
<a href="https://github.com/ahnshy/" target="_blank"><img src="https://img.shields.io/badge/GitHub-181717?logo=GitHub&logoColor=white"/></a>
<a href="https://www.facebook.com/ahnshy/" target="_blank"><img src="https://img.shields.io/badge/Facebook-blue?logo=facebook" /></a>
<a href="https://www.linkedin.com/in/hoseong-ahn-97057191/" target="_blank"><img src="http://img.shields.io/badge/-LinkedIn-0072b1?style=flat&logo=linkedin" /></a>
<a href="https://blog.naver.com/ahnshy" target="_blank"><img src="https://img.shields.io/badge/Naver_Blog-white?logo=naver" /></a>
<a href="https://ahnshy.tistory.com/" target="_blank"><img src="https://img.shields.io/badge/Tistory_Blog-red?logo=tistory" /></a>
<a href="https://velog.io/@ahnshy/" target="_blank"><img src="https://img.shields.io/badge/Velog-20C997?logo=velog&logoColor=white" /></a>
<a href="https://ahnshy.slack.com/" target="_blank"><img src="https://img.shields.io/badge/Slack-4A154B?logo=slack" /></a>
<a href="https://open.kakao.com/me/ahnshy" target="_blank"><img src="https://img.shields.io/badge/KakaoTalk-gray?logo=KakaoTalk" /></a>
<img src="https://img.shields.io/badge/Telegram-white?logo=Telegram" />
<br/>

<!-- Buymeacoffee Link -->
## Donation
<a href="https://www.buymeacoffee.com/27G6yAl/" target="_blank"><img src="https://img.shields.io/badge/Buy_me_a_coffee-grey?logo=buymeacoffee&logoColor=#FFDD00" /></a>
